class Game < ApplicationRecord
	  belongs_to :team
    belongs_to :opponent, class_name: "Team", foreign_key: "team_id"
    belongs_to :match

	validates :team_score, numericality: { only_integer: true, greater_than_or_equal_to: 0, less_than: 11 }, presence: true
	validates :opponent_score, numericality: { only_integer: true, greater_than_or_equal_to: 0, less_than: 11 }, presence: true

	validate :not_match_self
	validate :only_three
	before_save :set_winner
	after_save :update_match

	def winner 
			if team_score > opponent_score 
					team.name
			else 
					opponent.name
			end
	end

	def update_match
		if match.games.count >= 2
			array = match.games.pluck(:winner)
			winner = array.detect { |e| array.count(e) > 1 }
			return unless winner
			match.update_attributes(winner: winner)
			match.update_attributes(status: "Completed")
		end
	end

private	
	def set_winner
		self[:winner] = winner
	end

	def not_match_self
				return unless team_id == opponent_id
				errors.add :team , "cannot match itself"
	end

	def only_three
		return unless match.games.count >= 3
		errors.add :match , "cannot have more than 3 games"
	end

end
