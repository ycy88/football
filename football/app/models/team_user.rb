class TeamUser < ApplicationRecord
  belongs_to :team
  belongs_to :user

  validates :user_id, :uniqueness => {:scope=>:team_id}
	validate :check_user_number

private
    def check_user_number
        if self.team.users.count >= 2
					errors.add :team, "cannot have more than 2 users"
        end
    end

end