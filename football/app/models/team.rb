class Team < ApplicationRecord
    has_many :users, through: :team_users
    has_many :team_users, dependent: :restrict_with_exception

    has_many :opponents, through: :games
    has_many :games, dependent: :restrict_with_exception

    validates :name, presence: true
end
