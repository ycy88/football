class User < ApplicationRecord
    has_many :teams, through: :team_users
    has_many :team_users, dependent: :restrict_with_exception

    validates :first_name, presence: true
    validates :last_name, presence: true
end
