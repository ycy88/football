class Match < ApplicationRecord
	has_many :games, dependent: :restrict_with_exception

	before_create :init

private
	def init
		self.status = "In progress"
		self.winner = "Pending"
	end

end
