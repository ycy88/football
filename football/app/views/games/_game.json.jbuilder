json.extract! game, :id, :team_id, :opponent_id, :team_score, :opponent_score, :winner, :created_at, :updated_at
json.url game_url(game, format: :json)
