Create an application that will allow someone to do followings:
 Create and manage users
 Create teams (1 or 2 players in a team)
 Create and manage games between teams (Score and Winner/Loser)
 Show Teams ranking based on winning percentage
User:
 First and Last name
Team:
 Name
 Has 1 or 2 players and play one match together
Game:
 Has a score from 0-10
Match:
 Has one winning team
 Consist of 2-3 games (one team wins once they win 2 games)
