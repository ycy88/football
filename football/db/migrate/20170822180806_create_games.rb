class CreateGames < ActiveRecord::Migration[5.0]
  def change
    create_table :games do |t|
      t.references :team, foreign_key: true
      t.references :opponent, foreign_key: { to_table: :teams }
      t.integer :team_score
      t.integer :opponent_score
      t.string :winner

      t.timestamps
    end
  end
end
