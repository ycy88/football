Rails.application.routes.draw do
  resources :matches
  resources :games
	root 'teams#index'
  resources :teams
  resources :users
  resources :team_users
    
end
